@extends('bookSite::admin-layout')
@section('content')

<portfolio-properties inline-template>
    <div>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">Portfolio for {{ $portfolio->user->fullname() }}
                            <a href="/book-site/admin/portfolios/{{ $portfolio->id }}/edit" class="btn btn-sm btn-outline-secondary add-btn float-right">Add / remove properties</a>
                        </div>

                        <div class="card-body">
                            <div class="row mt-0">
                                <div class="col-sm-12">
                                    <table class="table m-b-none">
                                        <thead>
                                            <th>Property</th>
                                            <th></th>
                                            <th></th>
                                        </thead>

                                        <tbody>
                                            @foreach($properties as $property)
                                            <tr>
                                                <td>{{ $property->displayAddress() }}</td>

                                                @if ($property->hasCustomText($portfolio->id))
                                                <td>
                                                    <button class="btn btn-sm btn-outline-primary add-btn" v-on:click.prevent="editCustomText({{ $property }})">{{ $property->customText }}</button>
                                                </td>
                                                @else
                                                <td>
                                                    <button class="btn btn-sm btn-outline-success add-btn" v-on:click.prevent="editCustomText({{ $property }})">Add custom text</button>
                                                </td>
                                                @endif
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- Modal -->
        <div class="modal fade" id="edit-custom-text" tabindex="-1" role="dialog" aria-labelledby="editText" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">

                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Edit text</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>

              <form class="form-horizontal" method="POST" :action=" '/book-site/admin/custom-property-texts/' + property.id" enctype="multipart/form-data">
                {{ method_field('PUT') }}
                {{ csrf_field() }}

                <input type="hidden" name="portfolioId" value="{{ $portfolio->id }}">

                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="input-group mb-3">
                            <textarea rows="6" cols=100% name="text">@{{ property.customText }}</textarea>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

</div>
</portfolio-properties>


@endsection
