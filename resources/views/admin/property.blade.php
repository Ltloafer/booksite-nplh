@extends('bookSite::admin-layout')
@section('content')

<admin-property :images="{{ $property->images }}" :propertyid= "{{ $property->id }}" inline-template>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ $property->displayAddress() }}
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">Description
                                        <button class="btn btn-sm btn-outline-secondary add-btn float-right" data-toggle="modal" data-target="#edit-property-text"> Edit</button>
                                    </div>
                                    <div class="card-body">
                                        <p>{{ $property->description }} </p>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row mt-5">
                            <div class="col-sm-12">
                                <table class="table m-b-none">
                                    <thead>
                                        <th>Images</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </thead>

                                    <tbody>
                                        <tr v-for="image in orderedAdminImages">
                                            <td>@{{ image.id }}</td>
                                            <td v-if="property.title_image_id == image.id">
                                                Header image
                                            </td>
                                            <td v-else>
                                            </td>
                                            <!-- <td>@{{ image.type }}</td> -->
                                            <td>@{{ image.filename }}</td>

                                            <td> 
                                                <img :src=" '/storage/images/' + image.property_id + '/' + image.filename" height=150px width=150px alt="..." class="img-thumbnail img-fluid">
                                            </td>

                                            <td> 
                                                <button type="submit" class="btn btn-outline-success btn-sm" v-on:click="moveImageUp(image)"><i class="fas fa-arrow-alt-circle-up fa-lg"></i></button>

                                                <button type="submit" class="btn btn-outline-primary btn-sm ml-4" v-on:click="moveImageDown(image)"><i class="fas fa-arrow-alt-circle-down fa-lg"></i></button>
                                            </td>

                                            <td v-if="property.title_image_id == image.id">
                                            </td>
                                            <td v-else>
                                                   <button type="submit" class="btn btn-outline-secondary btn-sm ml-4" v-on:click="setHeaderImage(image)"><i class="fas fa-image fa-lg"></i></button>
                                            </td>

                                            <td>
                                                <button type="submit" class="btn btn-outline-danger btn-sm" style="float:right" v-on:click="removeImage(image)">Remove</button>
                                            </td>

                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <button class="btn btn-sm btn-outline-secondary add-btn float-right" data-toggle="modal" data-target="#add-image">Add image</button>

                    </div>
                </div>
            </div>
            @include('bookSite::admin.edit-text')
            @include('bookSite::admin.add-image')
        </div>
    </div>
</admin-property>

@endsection
