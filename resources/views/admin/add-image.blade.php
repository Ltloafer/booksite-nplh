<!-- Modal -->
<div class="modal fade" id="add-image" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add photo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-horizontal" method="POST" action="/book-site/admin/properties/{{ $property->id}}/images" enctype="multipart/form-data">
                <input type="hidden" name="propertyId" value="{{ $property->id }}">

                {{ csrf_field() }}

                <div class="modal-body">
                    <div class="container-fluid">

                      <div class="input-group mb-3">
                        <!--   <div class="input-group-prepend">
                            <span class="input-group-text">Upload</span>
                        </div> -->

                        <div class="custom-file">
                            <input multiple="multiple" type="file" class="custom-file-input" id="inputGroupFile01" name="photos[]">
                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</div>
</div>


