@extends('bookSite::admin-layout')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Admininstration
                    <button class="btn btn-sm btn-outline-secondary add-btn float-right" data-toggle="modal" data-target="#add-property"> Add a property +</button>
                </div>

                <div class="card-body">
                    <table class="table m-b-none">
                        <thead>
                            <th>Property</th>
                        </thead>

                        <tbody>
                            @foreach($properties as $property)
                            <tr>
                                <td>{{ $property->address }}</td>
                                <td><a href="/book-site/admin/properties/{{ $property->id}}" class="btn btn-outline-secondary btn-sm">View</a></td>
                         </tr>
                         @endforeach
                     </tbody>
                 </table>
             </div>
         </div>
     </div>
 </div>

@include('bookSite::admin.add-property')

</div>

@endsection
