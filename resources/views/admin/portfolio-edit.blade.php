@extends('bookSite::admin-layout')
@section('content')

<portfolio-properties inline-template>
    <div>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">Portfolio for {{ $portfolio->user->fullname() }} 
                            <a href="/book-site/admin/portfolios/{{ $portfolio->id }}" class="btn btn-sm btn-outline-secondary add-btn float-right">Done</a>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <table class="table table-condensed">
                                        <thead>
                                            <tr>
                                                <th>Properties</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($properties as $property)
                                            <tr>
                                                <td>{{ $property->displayAddress() }}</td>
                                                <td> 
                                                    <form class="form-horizontal" method="POST" action="/book-site/admin/portfolio/{{ $portfolio->id }}/properties">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="propertyId" value="{{ $property->id }}">
                                                        <button type="submit" class="btn btn-outline-primary btn-sm">Add</button>
                                                    </form>
                                                </td>         
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>     
                                </div>

                                <div class="col-sm-6">
                                    <table class="table m-b-none">
                                        <thead>
                                            <th>Portfolio properties</th>
                                            <th></th>
                                        </thead>
                                        <tbody>
                                            @foreach($portfolioProperties as $property)
                                            <tr>
                                                <td>{{ $property->displayAddress() }}</td>
                                                <td> 
                                                    <form class="form-horizontal" method="POST" action="/book-site/admin/portfolio/{{ $portfolio->id}}/properties/{{ $property->id}}">
                                                        @method('DELETE')
                                                        {{ csrf_field() }}
                                                        <button type="submit" class="btn btn-outline-danger btn-sm">Remove</button>
                                                    </form>
                                                </td>         
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</portfolio-properties>

@endsection
