<?php

namespace LtLoafer\bookSite\Controllers\Admin;

use App\Property;
use App\PropertyImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PropertyImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Property $property)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Property $property, Request $request)
    {
         try 
        {
            $property->storeImages($request);

        } catch (\Exception $e) {

            return back()->with('alert_message', 'This could not be saved!');
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Property $property, PropertyImage $image)
    {
        $image->delete();

        return;
    }

    public function setPropertyImagePositioning()
    {
        $properties = Property::all();

        foreach ($properties as $property) {

            $positionNo = 1;

            foreach ($property->images as $image) {
                $image->position_no = $positionNo;
                $image->save();
                $positionNo = $positionNo + 1;
            }
        }
    }

    public function moveImagePositionUp(PropertyImage $image)
    {
        $imagePostionNo = $image->position_no;

        $imageAbove = PropertyImage::where('property_id', $image->property_id)
        ->where('position_no', '<', $imagePostionNo)
        ->orderBy('position_no')
        ->get()
        ->last();

        if (! $imageAbove) {
            return;
        }

        $imageAbovePosition = $imageAbove->position_no;
        $imageAbove->position_no = $imagePostionNo;
        $imageAbove->save();

        $image->position_no = $imageAbovePosition;
        $image->save();

        return $image;
    }

    public function moveImagePositionDown(PropertyImage $image)
    {
        $imagePostionNo = $image->position_no;

        $imageBelow = PropertyImage::where('property_id', $image->property_id)
        ->where('position_no', '>', $imagePostionNo)
        ->orderBy('position_no')
        ->get()
        ->first();

        if (! $imageBelow) {
            return;
        }

        $imageBelowPosition = $imageBelow->position_no;
        $imageBelow->position_no = $imagePostionNo;
        $imageBelow->save();

        $image->position_no =  $imageBelowPosition;
        $image->save();

        return $image;
    }

      public function getProperty(Property $property)
    {
        return $property;
    }

    public function getImagesForProperty(Property $property)
    {
        return $property->images;
    }

    public function removeImage(PropertyImage $image)
    {
        $image->delete();
        
        return;
    }

    public function setHeaderImage(PropertyImage $image)
    {
        $property = $image->property;

        $property->title_image_id = $image->id;
        $property->save();

        return $property;
    }
}
