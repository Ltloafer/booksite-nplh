<?php

namespace LtLoafer\bookSite\Controllers\Admin;

use App\Property;
use App\Portfolio;
use App\PortfolioProperty;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PortfolioPropertiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Portfolio $portfolio, Request $request)
    {
        $portfolioProperty = new PortfolioProperty();
        $portfolioProperty->property_id = $request->input('propertyId');
        $portfolioProperty->portfolio_id = $portfolio->id;
        $portfolioProperty->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Property $property)
    {
        return view('admin.property')->with('property', $property);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Portfolio $portfolio, Property $property)
    {
        $portfolioProperty = PortfolioProperty::where('portfolio_id', $portfolio->id)
        ->where('property_id', $property->id)
        ->first();

        if ($portfolioProperty) {
            $portfolioProperty->delete();
        }

        return back();
    }
}
