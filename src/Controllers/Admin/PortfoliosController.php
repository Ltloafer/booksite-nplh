<?php

namespace LtLoafer\bookSite\Controllers\Admin;

use App\User;
use App\Property;
use App\Portfolio;
use App\PortfolioProperty;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PortfoliosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portfolios = Portfolio::all();

        $users = User::all();

        return view('admin.portfolios')->with('portfolios', $portfolios)->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $portfolio = new Portfolio();

        $portfolio->user_id = $request->input('userId');
        $portfolio->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Portfolio $portfolio)
    {
        // $properties = Property::all();

        $portfolioProperties = $portfolio->properties;

        $propertyIds = PortfolioProperty::where('portfolio_id', $portfolio->id)->pluck('property_id');

        $properties = Property::whereIn('id', $propertyIds)->get();

        foreach ($properties as $property) {
            $property->customText = $property->getCustomText($portfolio->id);
        }

        return view('bookSite::admin.portfolio')->with('portfolio', $portfolio)->with('properties', $properties);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Portfolio $portfolio)
    {
        $properties = Property::all();

        $portfolioPropertyIds = PortfolioProperty::where('portfolio_id', $portfolio->id)->pluck('property_id');

        $portfolioProperties = Property::whereIn('id', $portfolioPropertyIds)->get();

        $properties = Property::whereNotIn('id', $portfolioPropertyIds)->get();

        return view('bookSite::admin.portfolio-edit')->with('portfolio', $portfolio)->with('properties', $properties)->with('portfolioProperties', $portfolioProperties);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
