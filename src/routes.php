<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your this package. These
| routes are loaded by the package ServiceProvider. 
*/

Route::group(['prefix' => 'book-site/admin', 'middleware' => ['admin']], function () {

	Route::get('/getProperty/{property}', 'Admin\PropertyImagesController@getProperty');
	Route::get('/getPropertyImages/{property}', 'Admin\PropertyImagesController@getImagesForProperty');
	Route::post('/moveImagePositionUp/{image}', 'Admin\PropertyImagesController@moveImagePositionUp');
	Route::post('/moveImagePositionDown/{image}', 'Admin\PropertyImagesController@moveImagePositionDown');
	Route::post('/setHeaderImage/{image}', 'Admin\PropertyImagesController@setHeaderImage');
	Route::post('/removeImage/{image}', 'Admin\PropertyImagesController@removeImage');

	Route::resource('/dashboard', 'Admin\DashboardController');
	Route::resource('/users', 'Admin\UsersController');
	Route::resource('/properties', 'Admin\PropertiesController');
	Route::resource('/portfolios', 'Admin\PortfoliosController');
	Route::resource('/portfolio.properties', 'Admin\PortfolioPropertiesController');
	Route::resource('/properties.images', 'Admin\PropertyImagesController');
	Route::resource('/property-text', 'Admin\PropertyTextController');
	Route::resource('/custom-property-texts', 'Admin\CustomPropertyTextController');
});




