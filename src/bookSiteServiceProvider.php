<?php

namespace LtLoafer\bookSite;

use Illuminate\Support\ServiceProvider;

class bookSiteServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
            // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'ltloafer');
            // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'bookSite');

        $this->app['router']->namespace('LtLoafer\\bookSite\\Controllers')
        ->middleware(['web'])
        ->group(function () {
            $this->loadRoutesFrom(__DIR__ . '/routes.php');
        });

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/booksite.php', 'booksite');

        // Register the service the package provides.
        $this->app->singleton('booksite', function ($app) {
            return new bookSite;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['booksite'];
    }
    
    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole()
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/booksite.php' => config_path('booksite.php'),
        ], 'booksite.config');

        $this->publishes([
            __DIR__.'/../resources/js/components' => base_path('resources/js/components/bookSite'),
        ], 'bookSite-components');

        // Publishing the views.
        /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/ltloafer'),
        ], 'booksite.views');*/

        // Publishing assets.
        /*$this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/ltloafer'),
        ], 'booksite.views');*/

        // Publishing the translation files.
        /*$this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/ltloafer'),
        ], 'booksite.views');*/

        // Registering package commands.
        // $this->commands([]);
    }
}
